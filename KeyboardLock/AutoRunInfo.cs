using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KeyboardLock
{
    public class AutoRunInfo
    {
        public bool AutoLock { get; set; }
        public bool AutoRun { get; set; }
        public string LockPw { get; set; }
        public string UnlockPw { get; set; }
    }
}
