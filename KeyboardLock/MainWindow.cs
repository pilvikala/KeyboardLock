using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using Gma.UserActivityMonitor;
using KeyboardLock.Properties;
using System.Threading;

namespace KeyboardLock
{
    public partial class MainWindow : Form
    {

        System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();
        bool locked = false;
        string unlockPhrase = "unlock";
        string lockPhrase = "cake";
        string enteredPhrase = string.Empty;

        public AutoRunInfo Autorun { get; set; }

        public MainWindow()
        {
            Trace.WriteLine("MainWindow()");

            Autorun = new AutoRunInfo();

            if (Environment.CommandLine.Contains("-cs"))
            {
                Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("CS-cz");
                Trace.WriteLine("Switching UI to Czech");
            }

            if (Environment.CommandLine.Contains("-us"))
            {
                Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("EN-us");
                Trace.WriteLine("Switching UI to EN-us");
            }

            InitializeComponent();

            unlockPhrase = Settings.Default.UnlockPassword;
            lockPhrase = Settings.Default.LockPassword;
            Trace.WriteLine("unlockPhrase set to " + unlockPhrase);
            Trace.WriteLine("lockPhrase set to " + lockPhrase);

        }

        protected override void OnLoad(EventArgs e)
        {
            Trace.WriteLine("OnLoad");
            if (Autorun.AutoLock)
            {
                Trace.WriteLine("Autolock enabled");
                lockPasswordTextbox.Text = string.IsNullOrEmpty(Autorun.LockPw) ? lockPhrase : Autorun.LockPw;
                StartHook();
                ToggleLock(true);
            }
            else if (Autorun.AutoRun)
            {
                Trace.WriteLine("AutoRun enabled");
                lockPasswordTextbox.Text = string.IsNullOrEmpty(Autorun.LockPw) ? lockPhrase : Autorun.LockPw;
                startButton_Click(this, EventArgs.Empty);
            }
            else
            {
                Trace.WriteLine("Standard mode");
                lockPasswordTextbox.Text = lockPhrase;
            }
            base.OnLoad(e);
        }

        private void ToggleLock(bool delayHide)
        {
            Trace.WriteLine("Toggle lock " + delayHide);
            locked = !locked;
            Trace.WriteLine("locked: " + locked);
            if (locked)
            {
                if (delayHide)
                {
                    BeginInvoke(new MethodInvoker(Hide));
                }
                else 
                {
                    Hide();
                }
            }
            else
            {
                EndHook();
                Show();
                Autorun.AutoLock = false;
            }
        }

        private void ToggleLock()
        {
            Trace.WriteLine("Toggle lock");
            ToggleLock(false);
        }

        void HookManager_MouseClickExt(object sender, MouseEventExtArgs e)
        {
            Trace.WriteLine("HookManager_MouseClickExt");
            if (locked)
            {
                Trace.WriteLine("    handled");
                e.Handled = true;
            }
        }

        void HookManager_KeyUp(object sender, KeyEventArgs e)
        {
            Trace.WriteLine("HookManager_KeyUp");
            if (locked)
            {
                Trace.WriteLine("    handled");
                e.Handled = true;
            }
        }

        void HookManager_KeyPress(object sender, KeyPressEventArgs e)
        {
            enteredPhrase += e.KeyChar;
            Trace.WriteLine(">" + e.KeyChar);
            if (!unlockPhrase.Contains(enteredPhrase) && locked)
            {
                enteredPhrase = string.Empty;
            }
            if (!lockPhrase.Contains(enteredPhrase) && !locked)
            {
                enteredPhrase = string.Empty;
            }
            Trace.WriteLine("Current text: " + enteredPhrase);
            if(enteredPhrase.EndsWith(unlockPhrase) && locked)
            {
                Trace.WriteLine("enteredPhrase \"" + enteredPhrase + "\" matches unlockPhrase \"" + unlockPhrase + "\", toggling lock");
                enteredPhrase = string.Empty;
                ToggleLock();
            }
            else if (enteredPhrase.EndsWith(lockPhrase) && !locked)
            {
                Trace.WriteLine("enteredPhrase \"" + enteredPhrase + "\" matches lockPhrase \"" + lockPhrase + "\", toggling lock");
                enteredPhrase = string.Empty;
                ToggleLock();
            }
            if (locked)
            {
                Trace.WriteLine("    handled");
                e.Handled = true;
            }
        }

        void HookManager_KeyDown(object sender, KeyEventArgs e)
        {
            Trace.WriteLine("HookManager_KeyDown");
            if (locked)
            {
                Trace.WriteLine("    handled");
                e.Handled = true;
            }
        }

        private void StartHook()
        {
            Trace.WriteLine("StartHook");
            HookManager.KeyDown += new KeyEventHandler(HookManager_KeyDown);
            HookManager.KeyPress += new KeyPressEventHandler(HookManager_KeyPress);
            HookManager.KeyUp += new KeyEventHandler(HookManager_KeyUp);
            HookManager.MouseClickExt += new EventHandler<MouseEventExtArgs>(HookManager_MouseClickExt);
            
            UpdateControls(true);
        }

        

        private void EndHook()
        {
            Trace.WriteLine("EndHook");
            HookManager.KeyDown -= new KeyEventHandler(HookManager_KeyDown);
            HookManager.KeyPress -= new KeyPressEventHandler(HookManager_KeyPress);
            HookManager.KeyUp -= new KeyEventHandler(HookManager_KeyUp);
            HookManager.MouseClickExt -= new EventHandler<MouseEventExtArgs>(HookManager_MouseClickExt);
            
            UpdateControls(false);
        }

        private void UpdateControls(bool started)
        {
            Trace.WriteLine("UpdateControls " + started);
            startButton.Visible = !started;
            stopButton.Visible = started;
            helpLabel1.Visible = !started;
            helpLabel2.Visible = started;
            lockPasswordTextbox.Enabled = !started;
            
        }

        private void startButton_Click(object sender, EventArgs e)
        {
            Trace.WriteLine("startButton_Click");
            StartHook();
        }

        private void stopButton_Click(object sender, EventArgs e)
        {
            Trace.WriteLine("stopButton_Click");
            EndHook();
        }

        private void lockPasswordTextbox_TextChanged(object sender, EventArgs e)
        {
            lockPhrase = lockPasswordTextbox.Text.ToLower();
            unlockPhrase = lockPasswordTextbox.Text.ToLower();
            startButton.Enabled = !string.IsNullOrEmpty(lockPasswordTextbox.Text);
            Trace.WriteLine("lockPhrase and unlockPhrase set to " + lockPhrase);
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            Trace.WriteLine("Closing");

            Settings.Default.LockPassword = lockPhrase;
            Settings.Default.UnlockPassword = unlockPhrase;
            Settings.Default.Save();

            Trace.Flush();
            base.OnClosing(e);
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            using (About about = new About())
            {
                TopMost = false;
                about.ShowDialog();
                TopMost = true;
            }
        }
    }
}
