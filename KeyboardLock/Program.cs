using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Threading;

namespace KeyboardLock
{
    static class Program
    {
        static TextWriterTraceListener listener;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.ThreadException += new System.Threading.ThreadExceptionEventHandler(Application_ThreadException);

            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
            var window = new MainWindow();
            var arguments = GetArguments(args);

            if (arguments.ContainsKey("autolock"))
            {
                window.Autorun.AutoLock = true;
                window.Visible = false;
            }
            if (arguments.ContainsKey("autorun"))
            {
                window.Autorun.AutoRun = true;
            }

            if (arguments.ContainsKey("enablelogging"))
            {
                string path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "keylock.log");
                listener = new TextWriterTraceListener(path);
                Trace.Listeners.Add(listener);
                Trace.WriteLine("KeyboardLock starting");
            }

            if (arguments.ContainsKey("l"))
            {
                window.Autorun.LockPw = arguments["l"];
                window.Autorun.UnlockPw = arguments["l"];
            }

            var version = Assembly.GetExecutingAssembly().GetName().Version;
            Trace.WriteLine("Version: " + version);

            foreach (var arg in arguments)
            {
                Trace.WriteLine(arg.Key + ": " + arg.Value);
            }

            Application.Run(window);
            if (listener != null)
            {
                listener.Close();
            }
        }

        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Trace.WriteLine("CurrentDomain_UnhandledException:" + e.ExceptionObject);
            Trace.Flush();
            if (listener != null)
            {
                listener.Close();
            }
        }

        static Dictionary<string, string> GetArguments(string[] args)
        {
            Dictionary<string, string> arguments = new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase);
            int i = 0;
            do
            {
                if (args.Length > 0 && args[i].StartsWith("-") && args.Length > (i + 1) && !args[i + 1].StartsWith("-"))
                {
                    arguments.Add(args[i].Trim('-'), args[i + 1]);
                    i += 2;
                }
                else if (args.Length > 0 && args[i].StartsWith("-"))
                {
                    arguments.Add(args[i].Trim('-'), "");
                    i++;
                }
            } while (i < args.Length);
            return arguments;
        }

        static void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
        {
            Trace.WriteLine("Application_ThreadException: " + e.Exception);
            Trace.Flush();
        }

        
    }
}
