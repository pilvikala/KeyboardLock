using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using System.Diagnostics;

namespace KeyboardLock
{
    public partial class About : Form
    {
        public About()
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            try
            {
                versionLabel.Text = Assembly.GetExecutingAssembly().GetName().Version.ToString();
            }
            catch
            {
                versionLabel.Text = "Error: insufficient rights";
            }
            base.OnLoad(e);
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                Process.Start(linkLabel1.Text);
            }
            catch
            {
                //ignore all
            }
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                Process.Start("mailto:" + linkLabel2.Text);
            }
            catch
            {
                //ignore all
            }
        }
    }
}
