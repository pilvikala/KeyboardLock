# KeyboardLock
A tool to lock keyboard and mouse until a password is entered. Useful when watching a movie with small babies or cats around.

Requires Gma.UserActivityMonitor: https://www.codeproject.com/Articles/7294/Processing-Global-Mouse-and-Keyboard-Hooks-in-C
